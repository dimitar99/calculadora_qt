#include "Calculadora.h"
#include "QDebug"

Calculadora::Calculadora(QWidget *parent):QDialog(parent){
    
    setupUi(this);

    connect(boton0, SIGNAL(clicked()),
            this, SLOT(slotDigito()));
    connect(boton1, SIGNAL(clicked()),
            this, SLOT(slotDigito()));
    connect(boton2, SIGNAL(clicked()),
            this, SLOT(slotDigito()));
    connect(boton3, SIGNAL(clicked()),
            this, SLOT(slotDigito()));
    connect(boton4, SIGNAL(clicked()),
            this, SLOT(slotDigito()));
    connect(boton5, SIGNAL(clicked()),
            this, SLOT(slotDigito()));
    connect(boton6, SIGNAL(clicked()),
            this, SLOT(slotDigito()));
    connect(boton7, SIGNAL(clicked()),
            this, SLOT(slotDigito()));
    connect(boton8, SIGNAL(clicked()),
            this, SLOT(slotDigito()));
    connect(boton9, SIGNAL(clicked()),
            this, SLOT(slotDigito()));
    connect(botonMas, SIGNAL(clicked()),
            this, SLOT(slotSumar()));
    connect(botonMenos, SIGNAL(clicked()),
            this, SLOT(slotRestar()));
    connect(botonPor, SIGNAL(clicked()),
            this, SLOT(slotMultiplicar()));
    connect(botonEntre, SIGNAL(clicked()),
            this, SLOT(slotDividir()));            
    connect(botonIgual, SIGNAL(clicked()),
            this, SLOT(slotCalcular()));
    connect(botonPunto, SIGNAL(clicked()),
            this, SLOT(slotDigito()));
    connect(botonBorrar, SIGNAL(clicked()),
            this, SLOT(slotBorrar()));
}

void Calculadora::slotDigito(){
    QObject * culpable = sender();
    QPushButton * bC = qobject_cast<QPushButton*>(culpable);
    QString nuevoTexto = labelDisplay->text()+bC->text();
    labelDisplay->setText(nuevoTexto);
}

void Calculadora::slotSumar(){
    numero1 = labelDisplay->text().toDouble();
    operacion = '+';
    labelDisplay->setText("");
}

void Calculadora::slotRestar(){
    numero1 = labelDisplay->text().toDouble();
    operacion = '-';
    labelDisplay->setText("");
}

void Calculadora::slotMultiplicar(){
    numero1 = labelDisplay->text().toDouble();
    operacion = '*';
    labelDisplay->setText("");
}

void Calculadora::slotDividir(){
    numero1 = labelDisplay->text().toDouble();
    operacion = '/';
    labelDisplay->setText("");
}

void Calculadora::slotCalcular(){
    numero2 = labelDisplay->text().toDouble();
    switch (operacion)
    {
    case '+':
        numero1 = numero1 + numero2;
        break;
    case '-':
        numero1 = numero1 - numero2;
        break;
    case '*':
        numero1 = numero1 * numero2;
        break;
    case '/':
        numero1 = numero1 / numero2;
        break;
    default:
        break;
    }
    labelDisplay->setText(QString::number(numero1));
}

void Calculadora::slotBorrar(){
    labelDisplay->setText("");
    numero1 = 0;
    numero2 = 0;
}