#ifndef CALCULADORA_H
#define CALCULADORA_H
#include "QDialog"
#include "ui_Calculadora.h"

class Calculadora : public QDialog, public Ui::Calculadora{
Q_OBJECT
public:
    Calculadora(QWidget *parent = 0);
public slots:
    void slotDigito();
    void slotSumar();
    void slotRestar();
    void slotMultiplicar();
    void slotDividir();
    void slotCalcular();
    void slotBorrar();
private:
    char operacion = 'a';
    double calculo; 
    double numero1, numero2;
};

#endif